function initSortableListPlugin(POSTMODULE_URL, depth) {
  depth = depth || 1;

  $('ol.sortable').nestedSortable({
		forcePlaceholderSize: true,
		handle: 'div',
		helper:	'clone',
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 50,
		tolerance: 'pointer',
		toleranceElement: '> div',
		maxLevels: depth,

		isTree: true,
		expandOnHover: 700,
		startCollapsed: false,
		
		sortChange: function() {
		  setTimeout(sync, 1000)
		}
	});
	
	$('.disclose').on('click', function() {
		$(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
	})
	
	$('a.del-btn').click(function() {
	  if(confirm('Are you sure?')) {
	    var me = $(this)
	    $.post(POSTMODULE_URL + 'remove/', { id: me.data().id }, function(r) {
	      if(r.deleted) me.parent().parent().remove();
	    })
	  }
	})
	
  function sync(){
		serialized = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
		//console.log(serialized)
		$.post(POSTMODULE_URL + 'reorder/', { data: JSON.stringify(serialized), post: true }, function(r) {
      if(r.error) alert(r.error)
    })
	}
}
