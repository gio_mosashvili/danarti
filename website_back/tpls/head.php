<?php
  $data->title = isset($data->title) ? $data->title : L('default_title');
  $data->description = isset($data->description) ? $data->description : L('default_description');
  $data->keywords = isset($data->keywords) ? $data->keywords : L('default_keywords');
  $data->site_name = isset($data->site_name) ? $data->site_name : L('default_site_name');
  $data->canonical = isset($data->canonical) ? $data->canonical : '';
  $data->image = isset($data->image) ? $data->image : '';
  $data->alternates = isset($data->alternates) ? $data->alternates : array();
  $data->css = isset($data->css) ? $data->css : array();
?>
<!DOCTYPE html>
  <html lang="<?= Lang::$lang ?>" class="no-js">
  <head>
    <title><?= $data->title ?></title>
    <base href="<?= ROOT_URL ?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">

    <?php
      foreach($data->alternates as $key => $link) { if($key == Lang::$lang) continue; ?>
      <link rel="alternate" hreflang="<?= $key ?>" href="<?= $link ?>" />
    <?php } ?>

    <?php
    /* Include google-verification-code */
      $googleVerificationCode = Settings::getSetting('google-verification-code', 'seo');
      if($googleVerificationCode) {
        echo '<meta name="google-site-verification" content="'.substr($googleVerificationCode, 0, 50).'" />';
      }
    ?>

    <!-- DON'T FORGET TO UPDATE -->
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <link rel="mask-icon" href="/img/favicon/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/img/favicon/favicon.ico">
    <meta name="msapplication-config" content="/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#000000">

    <meta name="description" content="<?= $data->description ?>"/>
    <meta name="keywords"  content="<?= $data->keywords ?>"/>
    <meta name="resource-type" content="document"/>

    <meta property="og:title" content="<?= $data->title ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?= $data->canonical ?>" />
    <meta property="og:image" content="<?= $data->image ?>" />
    <meta property="og:site_name" content="<?= $data->site_name ?>" />

    <?php
      if(!DEBUG) {
        echo '<link href="css/min/all.css" rel="stylesheet" type="text/css" />';
      } else {
        echo '<link href="css/main.css" rel="stylesheet" type="text/css" />';
      }
    ?>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="canonical" href="<?= $data->canonical ?>" />
    <link rel="icon" type="image/png" href="img/fav.ico"/>
  </head>
  <body>

  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=927234504005569";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
    <script src="//maps.googleapis.com/maps/api/js?key=<?= GOOGLE_MAPS_API_KEY ?>" async="" defer="defer" type="text/javascript"></script>
<div class="page-wrap page-<?= $data->pageName; ?>">
