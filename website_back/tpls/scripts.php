<?php
  if(!DEBUG) {
    echo '<script src="js/min/all.js"></script>';
  } else { ?>
    <script src="js/libs/jquery.js"></script>
    <script src="js/libs/jquery.mobile.js"></script>
    <script src="js/libs/fancybox.js"></script>
    <script src="js/libs/jquery.fitvids.js"></script>
    <script src="js/libs/jquery.bxslider.js"></script>
    <script src="js/libs/owl.carousel.js"></script>
    <script src="js/libs/jquery.nicescroll.min.js"></script>
    <script src="js/custom/events.js"></script>
<?php } ?>
<script>
    siteUrl="<?= SITE_URL ?>";
</script>
