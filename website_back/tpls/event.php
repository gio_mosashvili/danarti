<?php $date = strtotime($data->event->date) ?>
<article class="singleEvent">

    <div class="event col-lg-8 col-sm-10 col-xs-12">
        <h2><?= $data->event->title ?></h2><br/>
                <div class="date">
                  <span id="dateOne" class="black"><?= date('d',$date) ?></span>
                  <span id="month" class="black"><?= L(date('F',$date)) ?></span>
                  <span id="year" class="black"><?= date('Y',$date) ?></span>

                </div>
                <p><?= $data->event->text ?></p>
              <a href="javascript:history.back()" class="back"><?= L('უკან დაბრუნება') ?></a>
    </div>
    <div class="eventImg col-lg-8 col-sm-10 col-xs-12">
        <div class="eventFrame">
          <?php if(isset($data->event->image)){ ?>
          <img src="<?= ROOT_URL.'uploads/events/'.$data->event->image ?>">
          <?php } ?>

        </div>
    </div>

     <?php if(isset($data->event->link) && !empty($data->event->link) ){ ?>
     <div class="eventImg col-lg-8 col-sm-10 col-xs-12">
        <h2><?= L('ვიდეო') ?></h2>
        <div class="eventFrame">
             <iframe src="https://player.vimeo.com/video/<?= $data->event->link ?>" width="100%" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <?php } ?>

</article>
