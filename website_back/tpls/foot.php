  <?php
    /* Include google-analytics-code */
    $googleAnalyticsId = Settings::getSetting('google-analytics-id', 'seo');
    if($googleAnalyticsId && Settings::getSetting('seo_enabled', 'seo')) { ?>
    <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?= $googleAnalyticsId ?>', 'auto');

		ga('send', 'pageview');
		</script>
  <?php } ?>
  <!--CDN links for TweenLite, CSSPlugin, and EasePack-->
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/CSSPlugin.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/easing/EasePack.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenLite.min.js"></script>
<script>

// TweenPlugin.activate([TransformMatrixPlugin]);



var $navbutton = $('.navicon');
var $lineX = $('.nav-desktop .line-one');
var $lineY = $('.nav-desktop .line-two');

var $lineDX = $('.nav-devices .line-one');
var $lineDY = $('.nav-devices .line-two');

var clicked = false;

$navbutton.click( function() {

   if(clicked == false){
     TweenLite.to($lineX, .5, {css:{transform:"matrix(0.7071, 0.7071, -0.7071, 0.7071, 102.4506, -35.32)"}, ease:Power2.easeOut});
     TweenLite.to($lineY, .5, {css:{transform:"matrix(0.7071, -0.7071, 0.7071, 0.7071, -47.4685, 97.4185)"}, ease:Power2.easeOut});

     TweenLite.to($lineDY, .5, {css:{transform:"matrix(0.7071, 0.7071, -0.7071, 0.7071, 82.0345, -130.0488)"}, ease:Power2.easeOut});
     TweenLite.to($lineDX, .5, {css:{transform:"matrix(0.7071, -0.7071, 0.7071, 0.7071, 33.9512, 149.9655)"}, ease:Power2.easeOut});


   }else{
    console.log("else");
    TweenLite.to($lineX, .5, {css:{transform:"matrix(1, 0, 0, 1, 0, 0)"}, ease:Power2.easeOut});
    TweenLite.to($lineY, .5, {css:{transform:"matrix(1, 0, 0, 1, 0, 0)"}, ease:Power2.easeOut});

    TweenLite.to($lineDX, .5, {css:{transform:"matrix(1, 0, 0, 1, 0, 0)"}, ease:Power2.easeOut});
    TweenLite.to($lineDY, .5, {css:{transform:"matrix(1, 0, 0, 1, 0, 0)"}, ease:Power2.easeOut});
   }
   clicked = !clicked;
});




</script>
  </body>
</html>
