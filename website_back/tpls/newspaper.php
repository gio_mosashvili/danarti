<?php $date = strtotime($data->newspaper->date) ?>
<section class="paperContent">
      <div class="img col-lg-6 col-sm-12 col-xs-12">
        <div class="frame">
          <img src="<?= ROOT_URL.'uploads/newspaper/'.$data->newspaper->image ?>"/>
        </div>
      </div>
      <div class="newspaper col-lg-6  col-sm-12 col-xs-12 ">
            <span class="number"><?= $data->newspaper->number ?></span>
            <div class="text">
                <h2><?= $data->newspaper->title ?></h2>
                <div class="date">
                  <span id="month" class="black"><?= L(date('F',$date)) ?></span>
                  <span id="year" class="black"><?= date('Y',$date) ?></span>
                </div>
                <nav>
                    <ul>
                      <?php foreach ($data->articles as $key => $value) { ?>
                        <li><a class="black" href="<?= SITE_URL.'article/'.URL::escapeUrl($value->title).'/'.$value->id ?>"><?= $value->title ?></a></li>
                      <?php } ?>
                    </ul>
                </nav>
                <a href="javascript:history.back()" class="back"><?= L('უკან დაბრუნება') ?></a>
            </div>
      </div>
  </section>
  <script>
  $(document).ready(function(){
    var paperHeight = window.innerHeight;
     $('.newspaper').height(paperHeight);
     });
  </script>
