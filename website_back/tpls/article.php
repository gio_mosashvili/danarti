<?php $date = strtotime($data->newspaper->date) ?>
<?php $images = json_decode($data->article->images) ?>
<section class="articleContent">
    <div class="img col-lg-6 col-sm-12 col-xs-12">
      <div class="frame" id="owl-article-slider">
        <?php foreach ($images as $key => $value) { ?>
          <div class="owl-image"><img class="articleImg" src="<?= ROOT_URL.'uploads/article/'.$value ?>"></div>
        <?php } ?>
      </div>
    </div>
    <div class="news col-lg-6 col-sm-12 col-xs-12">
          <div class="text">
              <h2><?= $data->newspaper->title ?></h2><br>
              <h1><?= $data->article->title ?></h1><br>
              <p><?= $data->article->text ?></p>
              <br>
              <?php if(isset($data->article->link)){ ?>
                <iframe src="https://player.vimeo.com/video/<?= $data->article->link ?>" width="100%" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
              <?php } ?>
              <a href="javascript:history.back()" class="back"><?= L('უკან დაბრუნება') ?></a>
          </div>
    </div>
</section>
