<article class="singleEvent">

    <div class="event col-lg-8 col-sm-10  col-xs-12">
        <h2><?= $data->about->title ?></h2><br/><br/>
            <?= $data->about->text ?><br>
          <a href="javascript:history.back()" class="back"><?= L('უკან დაბრუნება') ?></a>
    </div>

    <?php if(isset($data->about->image)){ ?>
    <div class="aboutImg col-lg-8 col-sm-10 col-xs-12">
        <div class="eventFrame">
            <img src="<?= ROOT_URL.'uploads/about/'.$data->about->image ?>">
        </div>
    </div>
    <?php } ?>

    <?php if(isset($data->about->link) && !empty($data->about->link)){ ?>
     <div class="aboutImg col-lg-8 col-sm-10  col-xs-12">
        <h2><?= L('ვიდეო') ?></h2>
        <div class="eventFrame eventVideo">
             <iframe src="https://player.vimeo.com/video/<?= $data->about->link ?>" width="100%" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <?php } ?>

</article>
