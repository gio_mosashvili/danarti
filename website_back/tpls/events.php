<section class="eventsLoop">
<?php foreach ($data->events as $key => $value) { $date = strtotime($value->date)?>

  <article class="events">
      <div class="eventImg col-lg-6 col-sm-12 col-xs-12">
          <div class="eventFrame">
              <img src="<?= ROOT_URL.'uploads/events/'.$value->image ?>">
          </div>
      </div>
      <div class="event col-lg-6 col-sm-12 col-xs-12">
          <h2><?= $value->title ?></h2><br>
                  <div class="date">
                    <span id="dateOne" class="black"><?= date('d',$date) ?></span>
                    <span id="month" class="black"><?= L(date('F',$date)) ?></span>
                    <span id="year" class="black"><?= date('Y',$date) ?></span>
                  </div>
                  <p><?= $value->description ?></p>
                  <a href="<?= SITE_URL.'event/'.URL::escapeUrl($value->title).'/'.$value->id ?>" class="back"><?=  L('იხილეთ სრულად') ?></a>
      </div>
  </article>

<?php } ?>
</section>
