<section class="paper" id="paper" onscroll="scroll();">
  <?php foreach ($data->newspapers as $key => $value) { $date = strtotime($value->date) ?>
    <article class=" " id="first">
      <div class="img col-lg-6 col-sm-12 col-xs-12">
        <div class="frame">
          <!-- <span id="one" class="line"><span class="circle" id="onecircle"></span></span>
          <span id="two" class="line"></span>
          <span id="three" class="line"></span>
          <span id="four" class="line"><span class="circle" id="twocircle"></span></span> -->
          <img src="<?= ROOT_URL.'uploads/newspaper/'.$value->image ?>"/>
        </div>
      </div>
      <div class="news col-lg-6 col-sm-12 col-xs-12" style="background-color: <?=  $value->color ? '#'.$value->color : '#eee' ?>">
            <span class="number"><?= $value->number ?></span>
            <div class="text">
                <h2><?= $value->title ?></h2>
                <div class="date home">
                  <span id="month"><?= L(date('F',$date)) ?></span>
                  <span id="year"><?= date('Y',$date) ?></span>
                </div>
                <p><?= $value->description ?></p>
                <a href="<?= SITE_URL.'newspaper/'.URL::escapeUrl($value->title).'/'.$value->id ?>"><?= L('იხილეთ სრულად') ?></a>
            </div>

    </article>
    <?php } ?>
  </section>

<script>
 $(document).load(function(){
   var articles = document.getElementsByTagName("article");
  // console.log( articles );
 });
 $(document).ready(function(){
      $(window).scroll(function(){

        var article = document.getElementsByTagName("article");

        for(var i=0; i<article.length; i++){
            var pos = article[i].getBoundingClientRect();
            var z = pos.top;
            var bottom = pos.bottom;
            var height = article[i].height;
            var windowHeight = window.innerHeight / 2;

            if(pos.top <= windowHeight){
                article[i].className = "active";
            }

            if(pos.top > windowHeight){
                 article[i].className = "passive";
            }
            if(pos.bottom <= windowHeight){
              article[i].className = "passiveImg";

            }
        }
      });

    });

  $(document).ready(function(){
       var article = document.getElementsByTagName("article");
       article[0].className="active";
  });

  </script>
