<?php
class DB extends DB_BASE {

	public static function getArticles() {
		$data = array();

		$sql = "SELECT ar.*, paper.number as pnumber
				FROM article_".Lang::$lang." ar
		        LEFT JOIN newspaper_".Lang::$lang." paper ON ar.paper_id = paper.id";

		$st = self::$pdo->query($sql);
		while($row = $st->fetchObject()) {
		  $row->pnumber = 'გაზეთის ნომერი - '.$row->pnumber;
		  $data[] = $row;
		}

		return $data;
	}
}


