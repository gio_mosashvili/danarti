<?php
class Page_Newspaper extends Page {
  private $data = null;

  public function __construct($request) {
    parent::__construct($request);
  }

  public function loadData() {
    $id = (int) $this->request->id;

    $newspaper = DB::get(array(
      'table' => 'newspaper',
      'query' => array('active' => 1,'id' => $id),
      'single' => true
    ));
    if(!$newspaper) throw new Exception("Error Processing Request", 1);


    $articles = DB::get(array(
      'table' => 'article',
      'query' => array('active' => 1,'paper_id' => $newspaper->id),
      'order' => 'date'
    ));


    $this->data = (object) array(
      'newspaper' => $newspaper,
      'articles' => $articles
    );

  }

  public function getTemplates() {
    $this->request->canonical = $this->getCanonical();
    $this->request->alternates = $this->getAlternates();
    $this->request->pageObject = $this;

    $this->request->data = $this->data;

    $pageHead = new PageHead();
    $pageScripts = new PageScripts();

    return array(
      $pageHead->init($this->request),
      PageHeader::init($this->request),
      (object) array( "tpl" => "newspaper", "data" => $this->data ),
      PageFooter::init($this->request),
      $pageScripts->init($this->request),
      (object) array( "tpl" => "foot", "data" => null )
    );
  }

  public function getCanonical() {
    return  SITE_URL."newspaper/".URL::escapeUrl($this->data->newspaper->title).'/'.$this->data->newspaper->id;
  }

  public function getLinksForSitemap($lang) {
    $news = DB::get(array(
      'table' => 'newspaper_'.$lang,
      'query' => array( 'active' => 1 ),
      'multiLang' => false
    ));

    $links = array();

    foreach($news as $article) {
      $links[] = array(
        'url' => SITE_URL.'newspaper/'.URL::escapeURL($article->title).'/'.$article->id.'/',
        'changefreq' => 'weekly',
        'priority' => '0.8'
      );
    }

    return $links;
  }
}
