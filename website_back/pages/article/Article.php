<?php
class Page_Article extends Page {
  private $data = null;

  public function __construct($request) {
    parent::__construct($request);
  }

  public function loadData() {

    $id = (int) $this->request->id;

    $article = DB::get(array(
      'table' => 'article',
      'query' => array('active' => 1,'id' => $id),
      'single' => true,
      'order' => 'date'
      // 'order' => 'id ASC'
    ));

    $newspaper = DB::get(array(
      'table' => 'newspaper',
      'query' => array('active' => 1,'id' => $article->paper_id),
      'single' => true
    ));

    $this->data = (object) array(
      'article' => $article,
      'newspaper' => $newspaper
    );

  }

  public function getTemplates() {
    $this->request->canonical = $this->getCanonical();
    $this->request->alternates = $this->getAlternates();
    $this->request->pageObject = $this;

    $this->request->data = $this->data;

    $pageHead = new PageHead();
    $pageScripts = new PageScripts();

    return array(
      $pageHead->init($this->request),
      PageHeader::init($this->request),
      (object) array( "tpl" => "article", "data" => $this->data ),
      PageFooter::init($this->request),
      $pageScripts->init($this->request),
      (object) array( "tpl" => "foot", "data" => null )
    );
  }

  public function getCanonical() {
    return  SITE_URL."article/".URL::escapeUrl($this->data->article->title).'/'.$this->data->article->id;
  }

  public function getLinksForSitemap($lang) {
    $news = DB::get(array(
      'table' => 'article_'.$lang,
      'query' => array( 'active' => 1 ),
      'multiLang' => false
    ));

    $links = array();

    foreach($news as $article) {
      $links[] = array(
        'url' => SITE_URL.'article/'.URL::escapeURL($article->title).'/'.$article->id.'/',
        'changefreq' => 'weekly',
        'priority' => '0.8'
      );
    }

    return $links;
  }
}
