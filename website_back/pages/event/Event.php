<?php
class Page_Event extends Page {
  private $data = null;

  public function __construct($request) {
    parent::__construct($request);
  }

  public function loadData() {


    $id = (int) $this->request->id;

    $event = DB::get(array(
      'table' => 'events',
      'query' => array('active' => 1,'id' => $id),
      'single' => true
    ));

    $this->data = (object) array(
      'event' => $event
    );

  }

  public function getTemplates() {
    $this->request->canonical = $this->getCanonical();
    $this->request->alternates = $this->getAlternates();
    $this->request->pageObject = $this;

    $this->request->data = $this->data;

    $pageHead = new PageHead();
    $pageScripts = new PageScripts();

    return array(
      $pageHead->init($this->request),
      PageHeader::init($this->request),
      (object) array( "tpl" => "event", "data" => $this->data ),
      PageFooter::init($this->request),
      $pageScripts->init($this->request),
      (object) array( "tpl" => "foot", "data" => null )
    );
  }

  public function getCanonical() {
    return  SITE_URL."event/".URL::escapeUrl($this->data->event->title).'/'.$this->data->event->id;
  }

  public function getLinksForSitemap($lang) {
    $news = DB::get(array(
      'table' => 'events_'.$lang,
      'query' => array( 'active' => 1 ),
      'multiLang' => false
    ));

    $links = array();

    foreach($news as $article) {
      $links[] = array(
        'url' => SITE_URL.'event/'.URL::escapeURL($article->title).'/'.$article->id.'/',
        'changefreq' => 'weekly',
        'priority' => '0.8'
      );
    }

    return $links;
  }
}
