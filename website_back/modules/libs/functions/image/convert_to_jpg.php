<?php
function cn_convert_to_jpg($src) {
  $currentExt = cn_get_extension($src);
  if($currentExt == 'jpg') return cn_get_filename($src);

  $target_jpg = cn_set_extension($src, 'jpg');
  $imgResource = imagecreatefromstring(file_get_contents($src));
  imagejpeg($imgResource, $target_jpg);
  unlink($src);
  return cn_get_filename($target_jpg);
}

function cn_get_filename($src) {
  $path = explode('/', $src);
  return $path[count($path) - 1];
}

function cn_set_extension($name, $new_ext) {
  $ext = explode('.', $name);
  $ext[count($ext) - 1] = $new_ext;
  $name = implode('.', $ext);
  return $name;
}
