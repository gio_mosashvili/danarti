<div class="formRow">
  <label><?= $cfg->label ?>:</label>
  <input id="datefield-id-<?= $cfg->field_name ?>" type="text" name="<?= $cfg->field_name ?>"<?= $cfg->readOnly ? ' readonly="readonly"' : '' ?> value="<?= $cfg->value ?>" />
</div>

<?php if($cfg->format == 'Y-m-d') { ?>
<script>
  $(function() {
    $( "#datefield-id-<?= $cfg->field_name ?>" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  })
</script>
<?php } else { ?>
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css" />
<script src="js/jquery.datetimepicker.js"></script>
<script>
  $(function() {
    $( "#datefield-id-<?= $cfg->field_name ?>" ).datetimepicker({
      format: "<?= $cfg->format ?>"
    });
  })
</script>
<?php } ?>
