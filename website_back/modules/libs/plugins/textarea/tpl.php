<div class="formRow">
  <div class="grid3"><label><?= $cfg->label ?>:</label></div>
  <div class="grid9">
    <textarea rows="<?= $cfg->rows ?>" cols="<?= $cfg->cols ?>" name="<?= $cfg->field_name ?>" placeholder="<?= $cfg->placeholder ?>"<?= $cfg->readOnly ? ' readonly="readonly"' : '' ?>><?= $cfg->value ?></textarea>
  </div>
  <div class="clear"></div>
</div>
