<div class="formRow">
  <div class="grid3"><label><?= $cfg->label ?>:</label></div>
  <div class="grid9"><input type="text" id="<?= $cfg->id ?>" name="<?= $cfg->field_name ?>" class="tags" value="<?= $cfg->value ?>" /></div>
  <div class="clear"></div>
</div>

<script type="text/javascript" src="js/plugins/forms/jquery.tagsinput.min.js"></script>
<script>
  $(".tags").tagsInput({width:"100%"});
</script>
