<div class="formRow">
    <div class="grid9 check">
        <input<?= $cfg->readOnly ? ' disabled="disabled"' : '' ?> type="checkbox" id="<?= $cfg->id ?>"<?= $cfg->value ? ' checked="checked"' : '' ?> name="<?= $cfg->field_name ?>" />
        <label for="<?= $cfg->id ?>" style="user-select: none; -webkit-user-select: none;" class="mr20"><?= $cfg->label ?></label>
    </div>
    <div class="clear"></div>
</div>

<script>
  $(function() {
    loadScript(["js/plugins/forms/jquery.uniform.js" ], onload);
    function onload() {
      $("#<?= $cfg->id ?>").uniform();
    }
  })
</script>
