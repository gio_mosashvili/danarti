<div class="widget" style="margin: 19px 16px;">
  <div class="whead"><h6><?= $cfg->label ?></h6><div class="clear"></div></div>
  <textarea id="<?= $cfg->id ?>" name="<?= $cfg->field_name ?>" cols="<?= $cfg->cols ?>" rows="<?= $cfg->rows ?>"><?= $cfg->value ?></textarea>
</div>

<script type="text/javascript" src="js/tinymce/all.min.js"></script>
<!-- <link type="text/css" rel="stylesheet" href="js/tinymce/all.min.css" /> -->

<script type="text/javascript" src="js/tinymce/tinymce.js"></script>

<script type="text/javascript">
window.staticInc = window.staticInc || 0;
window.staticInc++;

setTimeout(function() {
  tinymce.PluginManager.load('moxiecut', '<?= ROOT_URL.'admin_resources/' ?>js/tinymce/plugins/moxiecut/plugin.min.js');
  tinymce.init({
      selector: "textarea#<?= $cfg->id ?>",
      theme: "modern",
      //escape p
      force_br_newlines : true,
      force_p_newlines : false,
      forced_root_block : '',
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste moxiecut"
    ],
    toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link insertfile image media | forecolor | fontsizeselect",
    autosave_ask_before_unload: false,
      height: 500,
      relative_urls: false
  });
}, window.staticInc * 1000);
</script>
