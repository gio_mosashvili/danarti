<div class="formRow">
  <div class="cPicker" id="<?= $cfg->id ?>">
    <div class="color-picker" style="background-color: #<?= $cfg->value ?>"></div>
    <span><?= $cfg->label ?></span>
    <input type="hidden" name="<?= $cfg->field_name ?>" id="input_<?= $cfg->id ?>" />
  </div>
</div>

<script>
$(function() {
  loadScript(["js/plugins/ui/jquery.colorpicker.js" ], onload);
       
  function onload() {
    $('#<?= $cfg->id ?>').ColorPicker({
      color: '#<?= $cfg->value ?>',
      onShow: function (colpkr) {
        $(colpkr).fadeIn(500);
        return false;
      },
      onHide: function (colpkr) {
        $(colpkr).fadeOut(500);
        return false;
      },
      onChange: function (hsb, hex, rgb) {
        $('#<?= $cfg->id ?> div').css('backgroundColor', '#' + hex);
        $('#<?= $cfg->id ?> input').val(hex);
      }
    });
    
    $('#input_<?= $cfg->id ?>').val('<?= $cfg->value ?>');
  }
})
</script>
