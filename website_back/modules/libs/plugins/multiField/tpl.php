<?php
  $fields = json_decode($cfg->value);
  $fields = $fields ?: array();
?>

<div class="formRow">
  <label style="display: block; width: 100%; float: none;"><?= $cfg->label ?>:</label>
  <?php if(count($fields) == 0) { ?>
  <div class="size-count">
    <input<?= $cfg->readOnly ? ' readonly="readonly"' : '' ?> style="width: 40%; max-width: 300px" type="text" name="<?= $cfg->key_name ?>[]" placeholder="<?= $cfg->key_placeholder ?>" value="" /> :
    <input<?= $cfg->readOnly ? ' readonly="readonly"' : '' ?> style="width: 40%; max-width: 300px" type="text" name="<?= $cfg->val_name ?>[]" placeholder="<?= $cfg->val_placeholder ?>" value="" />
    <?php if(!$cfg->readOnly) { ?>
    <a href="javascript:void(0)" class="tablectrl_small bDefault tipS remove_btn_<?= $cfg->id ?>"><span class="iconb" data-icon=""></span></a>
    <?php } ?>
  </div>
  <?php } else {
    foreach($fields as $field) {
      foreach($field as $key => $val) {
  ?>
  <div class="size-count">
    <input<?= $cfg->readOnly ? ' readonly="readonly"' : '' ?> style="width: 40%; max-width: 300px" type="text" name="<?= $cfg->key_name ?>[]" placeholder="<?= $cfg->key_placeholder ?>" value="<?= $key ?>" /> :
    <input<?= $cfg->readOnly ? ' readonly="readonly"' : '' ?> style="width: 40%; max-width: 300px" type="text" name="<?= $cfg->val_name ?>[]" placeholder="<?= $cfg->val_placeholder ?>" value="<?= $val ?>" />
    <?php if(!$cfg->readOnly) { ?>
    <a href="javascript:void(0)" class="tablectrl_small bDefault tipS remove_btn_<?= $cfg->id ?>"><span class="iconb" data-icon=""></span></a>
    <?php } ?>
  </div>
  <?php } } } ?>
  <div class="clear" id="wrapper_<?= $cfg->id ?>"></div>
  <?php if(!$cfg->readOnly) { ?>
  <div class="grid5"><button type="button" class="buttonS bLightBlue" id="<?= $cfg->id ?>">დამატება</button></div>
  <?php } ?>
</div>


<script>
$(function() {
  $('#<?= $cfg->id ?>').click(function() {
    var div = $('<div class="size-count"></div>').html('<input style="width: 40%; max-width: 300px" type="text" name="<?= $cfg->key_name ?>[]" placeholder="<?= $cfg->key_placeholder ?>" value=""> : <input style="width: 40%; max-width: 300px" type="text" name="<?= $cfg->val_name ?>[]" placeholder="<?= $cfg->val_placeholder ?>" value=""> <a href="javascript:void(0)" class="tablectrl_small bDefault tipS remove_btn_<?= $cfg->id ?>"><span class="iconb" data-icon=""></span></a>')

    div.insertBefore($('#wrapper_<?= $cfg->id ?>'));
    initRemoveEvents();
  })
  initRemoveEvents();

  function initRemoveEvents() {
    $('.remove_btn_<?= $cfg->id ?>').click(function(){
      $(this).parent().remove()
    })
  }
})
</script>
