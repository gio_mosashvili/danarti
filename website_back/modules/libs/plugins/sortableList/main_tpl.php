<div class="sidePad">
  <a href="<?= MODULE_URL.$cfg->addBtn->url ?>" title="" class="sideB bLightBlue add-lang-text"><?= $cfg->addBtn->text ?></a>
</div>
<div class="clear"></div>

<div class="widget" style="width:99%; background: transparent; border: 0; margin-top: 0">
    <ol class="sortable">
      {InnerPlugin}
		</ol>
</div>

<div class="divider" style="margin-top: 15px; margin-bottom: 15px;"><span></span></div>

<script>
$(function() {
  loadScript(["js/jquery.mjs.nestedSortable.js", "js/libs/sortableList.js" ], onload);
  function onload() { initSortableListPlugin('<?= POSTMODULE_URL ?>', <?= $cfg->depth ?>); }
})
</script>
