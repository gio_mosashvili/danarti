<li id="menu_item_<?= $item->id ?>" class="mjs-nestedSortable-branch mjs-nestedSortable-expanded">
  <div><span class="disclose"><span></span></span><?= $item->{$cfg->displayField} ?>
    <a href="javascript:void(0)" class="tablectrl_small bDefault tipS del-btn" title="Delete" data-id="<?= $item->id ?>">
      <span class="icn"><i class="icn-close"></i></span>
    </a>
    <a href="<?= MODULE_URL.'edit/'.$item->id ?>" class="tablectrl_small bDefault tipS edit-btn" title="Edit">
      <span class="icn"><i class="icn-edit2"></i></span>
    </a>
  </div>

  <?= $childrenTplView ?>
</li>
