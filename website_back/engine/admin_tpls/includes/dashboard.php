<!--Statistic Js-->
<script type="text/javascript" src="js/plugins/charts/jquery.flot.js"></script>
<script type="text/javascript" src="js/charts/chart.js"></script>
<!--users js-->
<script type="text/javascript" src="js/plugins/tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(function() {
  oTable = $('.dTable').dataTable({
		"bJQueryUI": false,
		"bAutoWidth": false,
		"sPaginationType": "full_numbers",
		"sDom": '<"H"fl>t<"F"ip>'
	});
})
</script>

<ul class="middleNavR">
  <?php foreach($data->dashboard as $icon) { ?>
    <li><a href="<?= URL::parseLink($icon->link) ?>" title="<?= $icon->name ?>" class="tipN"><img src="<?= ROOT_URL ?>uploads/adminicons/<?= $icon->icon ?>" alt="<?= $icon->name ?>" /></a></li>
  <?php } ?>
</ul>
<div class="divider"></div>

<?php if(isset($CFG['stats']) && $CFG['stats']) { ?>
<div class="widget chartWrapper">
  <div class="whead"><h6>Charts</h6>
      <div class="titleOpt">
          <a href="#" data-toggle="dropdown"><span class="icos-cog3"></span><span class="clear"></span></a>
          <ul class="dropdown-menu pull-right">
            <li><a href="#"><span class="icos-add"></span>Add</a></li>
            <li><a href="#"><span class="icos-trash"></span>Remove</a></li>
            <li><a href="#" class=""><span class="icos-pencil"></span>Edit</a></li>
            <li><a href="#" class=""><span class="icos-heart"></span>Do whatever you like</a></li>
          </ul>
      </div>
      <div class="clear"></div>
  </div>
  <div class="body">
    <div class="chart"></div>
  </div>
</div>
<?php } ?>

<div class="widget">
  <div class="whead">
    <h6>Login statistics</h6>
    <div class="clear"></div>
  </div>
  <div id="dyn" class="hiddenpars">
    <a class="tOptions" title="Options"><img src="images/icons/options.png" alt="" /></a>
    <table cellpadding="0" cellspacing="0" border="0" class="dTable" id="dynamic">
    <thead>
      <tr>
        <th>Rendering engine<span class="sorting" style="display: block;"></span></th>
        <th>Browser</th>
        <th>Platform(s)</th>
        <th>Date</th>
      </tr>
      </thead>
      <tbody>
        <?php foreach($data->authlogs as $authlogs){
         echo" <tr class='gradeX'>
          <td>$authlogs->user</td>
          <td>$authlogs->browser</td>
          <td>$authlogs->platform</td>
          <td>$authlogs->date</td>
          </tr>";
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
function getMonthlyStats($data) {
  $allDays = array();

  for($i = -30; $i <= 0; $i++) {
    $key = date('Y-m-d', strtotime($i.' day'));
    $allDays[$key] = 0;
  }

  $javascriptArrayString = '';
  $maxY = 0;

  foreach($data as $item) {
    $allDays[$item->date] = $item->count;
  }

  ksort($allDays);

  foreach($allDays as $date => $count) {
    $javascriptArrayString .= '[new Date("'.$date.'"), '.$count.'],';
    $maxY = $maxY < $count ? $count : $maxY;
  }

  return array( 'jsString' => $javascriptArrayString, 'maxY' => $maxY );
}

$maxY = 0;
$stats = array();

if(isset($CFG['stats']) && $CFG['stats']) {
  foreach($CFG['stats'] as $label => $tableName) {
    $key = 'stats_'.$tableName;
    $tmpStat = getMonthlyStats($data->$key);
    $tmpStat['label'] = $label;
    $stats[] = $tmpStat;
    $maxY = max($tmpStat['maxY'], $maxY);
  }
}


?>


<script>
$(function() {

  var stats = [];

  <?php foreach($stats as $stat) { ?>
    stats.push({ data: [<?= $stat['jsString'] ?>], label: "<?= $stat['label'] ?>"});
  <?php } ?>

  var cfg = {
    stats: stats,
    yMin: 0,
    yMax: <?= $maxY + 1 ?>
  };

  drawGeneralChart(cfg);
})
</script>
