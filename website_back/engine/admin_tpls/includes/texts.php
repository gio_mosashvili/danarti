<style>
  textarea { height: 27px; resize: none; color: #000; }
</style>

<div class="scroll_wrapper">
  <div class="widget" style="width:98%; margin-left:10px">
   <table cellpadding="0" cellspacing="0" width="100%" class="tLight noBorderT editors" id="lang_texts_table">
      <thead><tr><td>ტექსტი</td><td><?= $CFG['LANG_NAMES'][$data->activeLang] ?> თარგმანი</td></tr></thead><thead></thead>
      <tbody>

      <?php foreach($data->allLangs as $k => $v) { ?>
        <tr>
          <td valign="top">
            <textarea class="noneditable key-prop" readonly="readonly"><?= $k ?></textarea>
          </td>
          <td valign="top">
            <textarea class="editable right-small"><?= isset($data->currentLangs->$k) ? $data->currentLangs->$k : ''; ?></textarea>
            <div class="saveDiv" style="display: none;">
              <a href="javascript:void(0)" style="font-weight:bold" class="btn-save">Save</a>&nbsp;&nbsp;&nbsp;
              <a href="javascript:void(0)" class="btn-cancel">Cancel</a>
            </div>
          </td>
          <?php if($data->allowAddDelete) { ?>
            <td class="tableActs noBorderB" style="width: 20px">
  				    <a href="javascript:void(0)" class="tablectrl_small bDefault tipS remove-lang-text" original-title="წაშლა"><span class="icn"><i class="icn-close"></i></span></a>
  			    </td>
  			  <?php } ?>
        </tr>
      <?php } ?>

      </tbody>
  </table>
  </div>

  <?php if($data->allowAddDelete) { ?>
    <div class="sidePad" style="width: 200px; font-size: 12px;">
      <a href="javascript:void(0)" title="" class="sideB bLightBlue add-lang-text">ტექსტის დამატება</a>
    </div>
  <?php } ?>
</div>
