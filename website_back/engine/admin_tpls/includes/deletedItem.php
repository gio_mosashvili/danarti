<?php 
if($data->error) {
  echo '<div class="nNote nFailure"><p>'.$data->error.'</p></div><br />';
}

if($data->item) {
  $date = $data->item->date;
  $username = $data->item->username;
  $metadata = json_decode($data->item->metadata);
?>
<fieldset>
  <div class="widget fluid">
    <div class="whead"><h6>Deleted</h6><div class="clear"></div></div>
    <div class="formRow" style="word-break: break-all;">
        <div class="grid3"><label>user:</label></div>
      <div class="grid9"><?= $username ?></div><br>
    </div>
    <div class="formRow" style="word-break: break-all;">
        <div class="grid3"><label>date:</label></div>
      <div class="grid9"><?= $date ?></div><br>
    </div>
    <?php foreach ($metadata as $key => $value) { ?>
    <div class="formRow" style="word-break: break-all;">
        <div class="grid3"><label><?= $key ?>:</label></div>
        <div class="grid9"><?= $value ?></div>
        <div class="clear"></div>
    </div>
  <?php }  ?>
  </div>
</fieldset>
<a href="<?= ADMIN_URL.'trash/' ?>" class="formRow" style="float: left; padding-left: 0px;">
  <form action="<?= ADMIN_URL.'trash' ?>" method="POST">
    <input type="hidden" name="restore" value="<?= $data->item->id ?>">
    <button type="submit" name="post" class="buttonS bBlue">Restore</button>
  </form>
  <div class="clear"></div>
</a>
  <a href="<?= ADMIN_URL.'trash'.'/delete/'.$data->item->id.'/' ?>" class="formRow" style="float: right;padding-right: 0px">
    <form action="<?= ADMIN_URL.'trash/' ?>" method="POST">
      <input type="hidden" name="delete" value="<?= $data->item->id ?>">
      <button type="submit" style="background: #F94E4E;border: 1px solid #C25555;-webkit-box-shadow: 0 1px 2px #726B6B" name="post" class="buttonS bBlue ">Delete forever</button>
    </form>
    <div class="clear"></div>
  </a>
<div class="clear"></div>
<?php } ?>
