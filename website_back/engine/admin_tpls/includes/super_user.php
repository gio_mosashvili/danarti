<style>
  textarea { height: 27px; resize: none; color: #000; }
</style>

<form action="<?= ADMIN_URL ?>super_user" method="POST">
  <fieldset>
    <div class="fluid">
      <div class="widget grid6" style="width: 100%">
        <div class="whead"><h6>CMS Settings</h6><div class="clear"></div></div>
        <?php
          foreach($data->settings as $key => $val) {
            if($key == 'DEBUG') { ?>
              <div class="formRow">
                <div class="grid3" style="width: 200px"><label><?= $key ?>: </label></div>
                <div class="grid9 on_off">
                  <div class="floatL mr10"><input type="checkbox" <?= $val == 1 ? ' checked="checked"' : '' ?> name="<?= $key ?>" /></div>
                </div>
                <div class="clear"></div>
              </div>
            <?php } else { ?>
              <div class="formRow">
                <div class="grid3" style="width: 200px"><label><?= $key ?>:</label></div>
                <div class="grid9"><input type="text" value="<?= $val ?>" name="<?= $key ?>" /></div><div class="clear"></div>
              </div>
            <?php }
          } ?>

          <div class="formRow">
            <input type='submit' style="margin: 0 auto; width: 100px; height: 40px;" value="Save" name="update_settings" class="sideB bLightBlue">
          </div>
      </div>
    </div>
  </fieldset>
</form>


<fieldset>
  <div class="fluid">
    <div class="widget grid6" style="width: 100%">
      <div class="whead"><h6>Truncate tables</h6><div class="clear"></div></div>

    <div class="formRow">
      <form action="<?= ADMIN_URL ?>super_user/" method="POST" style="float: left; margin-right: 30px;" onsubmit="return confirmTrashTruncate()">
        <input type='submit' style="margin: 0 auto; width: 160px; height: 40px;" value="Truncate trash" name="truncate-trash" class="sideB bLightBlue">
      </form>

      <form action="<?= ADMIN_URL ?>super_user/" method="POST" style="float: left; margin-right: 30px;" onsubmit="return confirmLogsTruncate()">
        <input type='submit' style="margin: 0 auto; width: 160px; height: 40px;" value="Truncate logs" name="truncate-logs" class="sideB bLightBlue">
      </form>

      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
</fieldset>

<script type="text/javascript" src="js/plugins/forms/jquery.ibutton.js"></script>
<script>
  $(function() {
    $('.on_off').iButton({ labelOn: "" , labelOff: "" });
  })

  function confirmTrashTruncate() {
    return confirm('Do you really want to truncate trash?');
  }

  function confirmLogsTruncate() {
    return confirm('Do you really want to truncate logs?');
  }
</script>
