<script type="text/javascript" src="js/plugins/tables/jquery.dataTables.js"></script>
<script type="text/javascript">
$(function() {
  oTable = $('.dTable').dataTable({
    "bJQueryUI": false,
    "bAutoWidth": false,
    "sPaginationType": "full_numbers",
    "sDom": '<"H"fl>t<"F"ip>'
  });
})
</script>
<div class="widget">
  <div class="whead">
    <h6>Deleted rows</h6>
    <div class="clear"></div>
  </div>
  <div id="dyn" class="hiddenpars">
    <a class="tOptions" title="Options"><img src="images/icons/options" alt="" /></a>
    <table cellpadding="0" cellspacing="0" border="0" class="dTable" id="dynamic">
    <thead>
      <tr>
        <th>User<span class="sorting" style="display: block;"></span></th>
        <th>Date</th>
        <th>table</th>
        <th>Action</th>
      </tr>
      </thead>
      <tbody>
        <?php
          foreach($data->trash as $item){  ?>
              <tr class='gradeX'>
              <td style='width: 33%'><?= $item->username ?></td>
              <td style='width: 33%'><?= $item->date ?></td>
              <td style='width: 33%'><?= $item->table ?></td>
              <td>
              <a href="<?= ADMIN_URL."trash/more/".$item->id ?>" class='tablectrl_small bDefault tipS L15' title='View'>
                <span class='icn'><i class='icn-search'></i></span>
              </a>
            </td>
            </tr>
            <?php } ?>
      </tbody>
    </table>
  </div>
</div>
