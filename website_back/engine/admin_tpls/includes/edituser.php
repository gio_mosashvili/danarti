<div style="width:98%; margin-left:10px" id="phpinfo">
  <form method="post" action="<?= ADMIN_URL.'users/'.($data->user->id ? 'edit/'.$data->user->id : 'add') ?>" class="main">
    <fieldset>
      <div class="widget grid9" style="margin-left:auto; margin-right:auto; float:none; width:500px">

        <?php if(isset($data->error)) echo '<div style="margin-top: 0;" class="nNote nFailure"><p>'.$data->error.'</p></div>'; ?>

	      <div class="whead"><h6>მომხმარებლის რედაქტირება</h6><div class="clear"></div></div>

	      <div class="formRow">
	        <div class="grid3"><label>მომხმარებლის სახელი:</label></div>
	        <div class="grid5"><input type="text" name="username" value="<?= $data->user->username ?>" placeholder="მომხმარებელი"></div>
	        <div class="clear"></div>
	      </div>
	      
	      <div class="formRow">
	        <div class="grid3"><label>ელ-ფოსტა:</label></div>
	        <div class="grid5"><input type="text" name="email" value="<?= $data->user->email ?>" placeholder="ელ-ფოსტა"></div>
	        <div class="clear"></div>
	      </div>
	      
	      <div class="formRow">
	        <div class="grid3"><label>პაროლი:</label></div>
	        <div class="grid5"><input type="password" name="pass" placeholder="პაროლი"></div>
	        <div class="clear"></div>
	      </div>
        
	      <div class="formRow">
	        <div class="grid3"><label>უფლებები:</label></div>
          <div class="grid5">
            <label><input type="checkbox" class="module-checkbox" value="modules"<?= in_array('modules', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;მოდულები</label>
            <?php foreach($data->modules as $mod) { ?>
              <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <label><input type="checkbox" class="submodule-checkbox" name="modules[]"<?= isset($mod->moduleid) ? ' checked="checked"' : '' ?> value="<?= $mod->id ?>">&nbsp;<?= $mod->name ?></label>
            <?php } ?>
            <br><label><input type="checkbox" value="texts"<?= in_array('texts', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;ტექსტები</label>
            <br><label>
            <input type="checkbox" value="texts_add_remove"<?= in_array('texts_add_remove', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;ტექსტების დამატება / წაშლა</label>
            <br><label><input type="checkbox" value="users"<?= in_array('users', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;მომხმარებლები</label>
            <br><label><input type="checkbox" value="stats"<?= in_array('stats', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;სტატისტიკა</label>
            <br><label><input type="checkbox" value="super_user"<?= in_array('super_user', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;Super User</label>
            <br><label><input type="checkbox" value="logs"<?= in_array('logs', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;ლოგები</label>
            <br><label><input type="checkbox" value="documentation"<?= in_array('documentation', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;დოკუმენტაცია</label>
            <br><label><input type="checkbox" value="trash"<?= in_array('trash', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;ნაგვის ყუთი</label>
            <br><label><input type="checkbox" value="seo"<?= in_array('seo', $data->rights) ? ' checked="checked"' : '' ?> name="roles[]">&nbsp;SEO</label>
            <br>
          </div>
	        <div class="clear"></div>
	      </div>
	      
	      <div class="formRow noBorderB">
	        <input type="hidden" value="<?= $data->user->id ?>" class="buttonS bLightBlue" name="userid">
	        <div class="grid3"><label>&nbsp;</label></div>
	        <div class="grid5"><input type="submit" value="შენახვა" class="buttonS bLightBlue" name="upsertUser"></div>
	        <div class="clear"></div>
	      </div>
      </div>
    </fieldset>
  </form>
  
<style type="text/css">
.formRow [class*="grid"] > label { float:none; display:inline }
.formRow { padding: 10px;  }
</style>

<script>
$(function() {
  $('.module-checkbox').click(function() {
    var val = $(this).prop('checked');
    
    $('.submodule-checkbox').prop({ checked: val })
  })
})
</script>
     
</div>
