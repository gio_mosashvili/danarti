<?php 
  $log = $data->log;
  $username = $log->username;
  $date = $log->date;

?>
<?php if($log->type == "content_edit"){

    $metadata = json_decode($log->metadata);

    $newDataArray = array();
    $oldDataArray = array(); 

    foreach ($metadata->new as $key => $value) {
      $newDataArray[$key] = $value;
    }

    foreach ($metadata->old as $key => $value) {
      $oldDataArray[$key] = $value;
    }
?>
  <fieldset style="width: 49%; margin-right: 2%; float: left;">
    <div class="widget fluid">
        <div class="whead"><h6>Old</h6><div class="clear"></div></div>
      <div class="formRow" style="word-break: break-all;">
          <div class="grid3"><label>user:</label></div>
        <div class="grid9"><?= $username ?></div><br>
      </div>
      <div class="formRow">
          <div class="grid3"><label>date:</label></div>
        <div class="grid9"><?= $date ?></div><br>
      </div>

      <?php
      foreach ($oldDataArray as $key => $value) {
       if ($value == $newDataArray[$key]) { ?>
        <div class="formRow" style="word-break: break-all;">
          <div class="grid3"><label><?= $key ?>:</label></div>
          <div class="grid9"><?= $value ?></div>
          <div class="clear"></div>
        </div>
      <?php }else{ ?>
      <div class="formRow" style="background: #FC9898; word-break: break-all;">
        <div class="grid3"><label><?= $key ?>:</label></div>
        <div class="grid9"><?= $value ?></div>
        <div class="clear"></div>
      </div>
      <?php }} ?>
    </div>
  </fieldset>

  <fieldset style="width: 49%; float: left;">
    <div class="widget fluid">
      <div class="whead"><h6>New</h6><div class="clear"></div></div>
      <div class="formRow" style="word-break: break-all;">
          <div class="grid3"><label>user:</label></div>
          <div class="grid9"><?= $username ?></div><br>
      </div>
      <div class="formRow" style="word-break: break-all;">
          <div class="grid3"><label>date:</label></div>
        <div class="grid9"><?= $date ?></div><br>
      </div>
      <?php
      foreach ($newDataArray as $key => $value) {
       if ($value == $oldDataArray[$key]) { ?>
        <div class="formRow" style="word-break: break-all;">
          <div class="grid3"><label><?= $key ?>:</label></div>
          <div class="grid9"><?= $value ?></div>
          <div class="clear"></div>
        </div>
      <?php }else{ ?>
        <div class="formRow" style="background: #BBFFAA; word-break: break-all;">
          <div class="grid3"><label><?= $key ?>:</label></div>
          <div class="grid9"><?= $value ?></div>
          <div class="clear"></div>
        </div>
      <?php }} ?>
    </div>
  </fieldset>

<?php }else{ ?>
<fieldset>
  <div class="widget fluid">
      <div class="whead"><h6>Changes</h6><div class="clear"></div></div>
    <div class="formRow" style="word-break: break-all;">
        <div class="grid3"><label>user:</label></div>
      <div class="grid9"><?= $username ?></div><br>
    </div>
    <div class="formRow" style="word-break: break-all;">
        <div class="grid3"><label>date:</label></div>
        <div class="grid9"><?= $date ?></div><br>
    </div>
        
    <?php
    $metadata = json_decode($log->metadata);
    foreach ($metadata as $data) {
      foreach ($data as $key => $value) {?>

      <div class="formRow" style="word-break: break-all;">
          <div class="grid3"><label><?= $key ?>:</label></div>
        <div class="grid9"><?= $value ?></div><br>
      </div>
    <?php  }}} ?>
  </div>
</fieldset>
<div class="clear"></div>
