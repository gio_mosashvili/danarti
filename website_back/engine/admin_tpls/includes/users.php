<style>

  textarea { height: 27px; resize: none; color: #000; }
</style>


<div class="widget" style="width:98%; margin-left:10px">

  <table cellpadding="0" cellspacing="0" width="100%" class="tLight noBorderT">
		  <thead>
		    <tr>
		      <td style="width:30px">&nbsp;</td><td>მომხმარებელი</td>
		      <td>ელ-ფოსტა</td>
		      <td style="width:80px">ქმედება</td>
		    </tr>
		  </thead>
     <tbody>
      <?php foreach($data->users as $user) { ?>
        <tr data-id="<?= $user->id ?>">
          <td class="noBorderB"><img src="images/svg/user.svg" width="30"></td>
          <td class="noBorderB"><?= $user->username ?></td>
          <td class="noBorderB"><?= $user->email ?></td>
          <td class="tableActs noBorderB">
				    <a href="<?= ADMIN_URL.'users/edit/'.$user->id ?>" class="tablectrl_small bDefault edit-btn tipS" original-title="რედაქტირება">
				      <!-- <span class="iconb" data-icon=""></span> -->
              <span class="icn"><i class="icn-edit2"></i></span>

				    </a>
				    <a href="javascript:void(0)" class="tablectrl_small bDefault tipS btn-remove-user" original-title="წაშლა">
				      <!-- <span class="iconb" data-icon=""></span> -->
              <span class="icn"><i class="icn-close"></i></span>
				    </a>
			    </td>
			  </tr>
			<?php } ?>
	  </tbody>
  </table>

</div>

<div class="sidePad" style="width: 200px; font-size: 12px;">
  <a href="<?= ADMIN_URL.'users/add' ?>" title="" class="sideB bLightBlue add-lang-text">მომხმარებლის დამატება</a>
</div>
