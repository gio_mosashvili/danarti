<?php
  if($data->error) {
    echo '<div class="nNote nFailure"><p>'.$data->error.'</p></div><br />';
  }
?>

<div class="wrapper" style="margin: 0">
  <?php if($data->seo_enabled) { ?>
  <form method="post" action="<?= ADMIN_URL.'seo/' ?>" class="main" enctype="multipart/form-data">
    <fieldset>
      <div class="widget grid9" style=" margin: 0 auto;">
        <div class="whead"><h6>Sitemap</h6><div class="clear"></div></div>

        <?php if($data->sitemap_exists) { ?>
        <div class="formRow">
          Sitemap last updated: <?= $data->sitemap_updated ?>
        </div>
        <?php } else { ?>
        <div class="formRow">
          Sitemap does not exists
        </div>
        <?php } ?>

        <div class="formRow">
          <input type="hidden" name="action" value="generate-sitemap" />
          <button type="submit" name="post" class="buttonS bBlue ">Generate sitemap</button>
          <div class="clear"></div>
        </div>
      </div>
    </fieldset>
  </form>

  <br />

  <form method="post" action="<?= ADMIN_URL.'seo/' ?>" class="main" enctype="multipart/form-data">
    <fieldset>
      <div class="widget grid9" style=" margin: 0 auto;">
        <div class="whead"><h6>General settings</h6><div class="clear"></div></div>

        <div class="formRow">
          <label>Google verification code:</label>
          <input type="text" name="google-verification-code" value="<?= $data->google_verification_code ?>" />
          <div class="clear"></div>
        </div>

        <div class="formRow">
          <label>Google analytics ID:</label>
          <input type="text" name="google-analytics-id" placeholder="UA-12345678-1" value="<?= $data->google_analytics_id ?>" />
          <div class="clear"></div>
        </div>

        <div class="formRow">
          <input type="hidden" name="action" value="update-settings" />
          <button type="submit" name="post" class="buttonS bBlue ">Update</button>
          <div class="clear"></div>
        </div>

      </div>
    </fieldset>
  </form>

  <br />
  <?php } ?>

  <form method="post" action="<?= ADMIN_URL.'seo/' ?>" class="main" enctype="multipart/form-data">
    <fieldset>
      <div class="widget grid9" style=" margin: 0 auto;">
        <div class="whead"><h6>SEO status</h6><div class="clear"></div></div>

        <?php if($data->seo_enabled) { ?>
        <div class="formRow">
          Seo enabled
        </div>

        <div class="formRow">
          <input type="hidden" name="action" value="disable-seo" />
          <button type="submit" name="post" class="buttonS bBlue ">Disable seo</button>
          <div class="clear"></div>
        </div>
        <?php } else { ?>
        <div class="formRow">
          Seo disabled
        </div>

        <div class="formRow">
          <input type="hidden" name="action" value="enable-seo" />
          <button type="submit" name="post" class="buttonS bBlue ">Enable seo</button>
          <div class="clear"></div>
        </div>
        <?php } ?>
      </div>
    </fieldset>
  </form>
</div>
