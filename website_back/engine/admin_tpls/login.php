<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>CONNECT CMS</title>
    <base href="<?= ROOT_URL.'admin_resources/' ?>" />
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
    <meta name="robots" content="noindex, nofollow">
    <!--[if IE]> <link href="js_css/ie.css" rel="stylesheet" type="text/css"><![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>

  <body class="home">
  <div id="connect-cms"><img src="<?= ROOT_URL.'admin_resources/images/svg/connect.png' ?>"></div>
  <div class="authorisation"><img src="<?= ROOT_URL.'admin_resources/images/svg/autorisation.svg' ?>"></div>
	  <form action="<?= ADMIN_URL.'login' ?>" method="post">
      <div class="fluid" style="margin-left:auto; margin-right:auto; width:302px">
        <?php if(isset($data->error) and $data->error) { ?>
          <div class="nNote nFailure"><p><?= $data->error ?></p></div>
        <?php } ?>

        <div class="login-page">
          <div class="logo-upload"><img src="<?= ROOT_URL.'img/cms_logo.svg'?>"></div>
          <!-- <div class="authorisation"><h6><?= L('ავტორიზაცია') ?></h6><div class="clear"></div></div> -->
              <div class="grid3"><label><?= L('მომხმარებელი') ?>:</label></div>
              <input class="username" type="text" name="username" value=""  /><div class="clear"></div>
              <div class="grid3"><label><?= L('პაროლი') ?>:</label></div>
              <input class="password"  type="password" name="pass" value=""  /><div class="clear"></div>
              <div style="margin-top: 10px"><div class="g-recaptcha" data-sitekey="<?=CAPTCHA_PUBLIC_KEY?>"></div></div><div class="clear"></div>
              <div class="grid5" style="margin-top: 20px; float: right;"><input type="submit" class="connect" name="submit" class="buttonS bLightBlue" value="CONNECT" /></div><div class="clear"></div>
        </div>
      </div>
    </form>
  </body>
</html>
