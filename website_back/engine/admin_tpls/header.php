<!-- Top line begins -->
<div id="top">
    <div class="wrapper">
        <a href="<?= ADMIN_URL ?>" title="" class="logo"><img src="images/svg/connect.png" alt="connect cms" /></a>

        <!-- Right top nav -->
        <div class="topNav">

            <ul class="userNav">
              <?php foreach($data->langs as $key => $val) { ?>
                <li><a href="<?= $val ?>" title="" class="language <?= $key ?>" style="opacity: <?= Lang::$lang == $key ? '1' : '0.5' ?>"></a></li>
              <?php } ?>
            </ul>
            <a title="" class="iButton"></a>
            <a title="" class="iTop"></a>
            <div class="topSearch">
                <div class="topDropArrow"></div>
                <form action="">
                    <input type="text" placeholder="search..." name="topSearch" />
                    <input type="submit" value="" />
                </form>
            </div>
        </div>

        <!-- Responsive nav -->
        <ul class="altMenu">
            <li><a href="<?= SITE_URL ?>" title="">Dashboard</a></li>
            <li><a href="messages.html" title="">Messages</a></li>
            <li><a href="statistics.html" title="">Statistics</a></li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<!-- Top line ends -->
