<!DOCTYPE html>
<html lang="<?= Lang::$lang ?>">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title><?= $data->title ?></title>
    <base href="<?= ROOT_URL.'admin_resources/' ?>" />

    <link href="css/styles.css" rel="stylesheet" type="text/css" />
    <!--[if IE]> <link href="css/ie.css" rel="stylesheet" type="text/css"><![endif]-->

    <link rel="icon" type="image/png" href="<?= ROOT_URL ?>img/favicon.ico"/>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/plugins/forms/ui.spinner.js"></script>
    <script type="text/javascript" src="js/plugins/forms/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/dashboard.js"></script>
    <script type="text/javascript" src="js/scriptsLoader.js"></script>
  </head>

  <body>
