<?php
class PageScripts {
  public function init($request) {
    $data = array(
      'jsfiles' => DEBUG ? @$request->jsFiles : array( 'js/min/'.strtolower($request->module).'.js' ),
    );

    return (object) array( "tpl" => "scripts", "data" => (object) $data );
  }
}
