<?php
class Mail {
  public static function sendPasswordUpdateMessage($data) {

    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->Host = SMTP_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTP_USERNAME;
    $mail->Password = SMTP_PASSWORD;

    $mail->From = 'donotreply@curatio.ge';
    $mail->FromName = 'Curatio';
    $mail->addAddress($data['addressee']);
    $mail->Subject = $data['subject'];
    $mail->Body    = $data['text'];

    $status = $mail->send();

    return $status;
  }

  public static function sendRecoveryMessage($data) {

    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->Host = SMTP_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTP_USERNAME;
    $mail->Password = SMTP_PASSWORD;

    $mail->From = 'donotreply@curatio.ge';
    $mail->FromName = 'Curatio';
    $mail->addAddress($data['addressee']);
    $mail->Subject = $data['subject'];
    $mail->Body    = $data['text'];

    $status = $mail->send();

    return $status;
  }

  public static function sendRegistrationMessage($data) {

    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->Host = SMTP_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTP_USERNAME;
    $mail->Password = SMTP_PASSWORD;

    $mail->From = 'donotreply@curatio.ge';
    $mail->FromName = 'Curatio';
    $mail->addAddress($data['addressee']);
    $mail->Subject = $data['subject'];
    $mail->Body    = $data['text'];

    $status = $mail->send();

    return $status;
  }

  public static function sendMailSenderMessage($data) {

    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = SMTP_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTP_USERNAME;
    $mail->Password = SMTP_PASSWORD;

    $mail->From = '';
    $mail->FromName = '';
    $mail->addReplyTo('');

    foreach ($data['addressee'] as $email) {
        $mail->addAddress($email);
    }

    $mail->Subject = "";
    $mail->Body    = $data['text'];

    return $mail->send();
  }

  public static function sendDoctorChangeMessage($data) {

    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->Host = SMTP_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTP_USERNAME;
    $mail->Password = SMTP_PASSWORD;

    $mail->From = 'donotreply@curatio.ge';
    $mail->FromName = 'Curatio';
    $mail->addAddress('lshubitidze@curatio.ge');
    $mail->Subject = L('_მოთხოვნა_პაციენტის_მიერ_ექიმის_შეცვლის_შესახებ_');
    $mail->Body  = L('_მომხმარებელი_').': ';
    $mail->Body .= $data['user']->name.' '.$data['user']->surname.' ';
    $mail->Body .= L('_მოითხოვა_ექიმის_შეცვლა_').'<br/>';
    $mail->Body .= L('_მიმდინარე_ექიმი_').': ';
    $mail->Body .= $data['current_doctor']->doctor_name.'<br/>';
    $mail->Body .= L('_ახალი_ექიმი_').': ';
    $mail->Body .= $data['new_doctor']->doctor_name.'<br/>';
    $mail->Body .= L('_მიზეზი_').': ';
    $mail->Body .= $data['reason'];

    $status = $mail->send();

    return $status;
  }
  public static function sendVisaRequestMessage($data) {


    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->Host = SMTP_HOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTP_USERNAME;
    $mail->Password = SMTP_PASSWORD;

    $mail->From = 'donotreply@curatio.ge';
    $mail->FromName = 'Curatio';
    $mail->addAddress('asaneblidze@curatio.ge');

    $mail->Subject = L('_მოთხოვნა_პაციენტის_მიერ_ექიმთან_ვიზიტის_შესახებ_');
    $mail->Body  = L('_მომხმარებელი_').': ';
    $mail->Body .= $data['user']->name.' '.$data['user']->surname.'<br/>';
    $mail->Body .= L('_ტელ_').' '.$data['user']->tel.'<br/>'.L('_ელ-ფოსტა_').' '.$data['user']->email.'<br/>';
    $mail->Body .= L('_მოითხოვა_ვიზიტი_ექიმთან_').'<br/>';
    $mail->Body .= L('_ექიმი_').': ';
    $mail->Body .= $data['doctor']->doctor_name.'<br/>';
    $mail->Body .= L('_თარიღი_').': ';
    $mail->Body .= date("d-M-Y", strtotime($data['visa_from'])).' / '.date("d-M-Y", strtotime($data['visa_to'])).'<br/>';

    $status = $mail->send();

    return $status;
  }

  public static function send($data) {
    if(empty($data['from']) || !$data['to'] || empty($data['subject']) || empty($data['message']) || empty($data['fromName'])) throw new Exception(L("incorrect_parameters_for_mailsender"));

    $mail = new PHPMailer(true);
    //$mail->SMTPDebug = 1;

    $mail->isSMTP();
    $mail->Host = MAIL_HOST;
    $mail->SMTPAuth = true;
    if(defined('MAIL_SMTP_SECURE')) $mail->SMTPSecure = MAIL_SMTP_SECURE;
    if(defined('MAIL_PORT')) $mail->Port = MAIL_PORT;

    $mail->Username = MAIL_USER;
    $mail->Password = MAIL_PASS;

    $mail->From = $data['from'];
    $mail->FromName = $data['fromName'];
    $mail->addAddress($data['to']);
    $mail->addReplyTo($data['from']);

    $mail->Subject = $data['subject'];
    $mail->Body = $data['message'];
    $mail->AltBody = strip_tags(str_replace('<br>', "\n", $data['message']));

    if(isset($data['attachment'])) {
      if(is_array($data['attachment'])) {
        foreach($data['attachment'] as $value){
          $mail->addAttachment($value['path'], $value['name']);
        }
      } else {
        $mail->addAttachment($data['attachment'], $data['attachmentName']);
      }
    }

    $mail->CharSet = 'UTF-8';

    $status = $mail->send();

    if (!$status) throw new Exception(L("unable_to_send_email"));

    return $status;
  }

}
