<?php
class Admin_PageHeader {
  public static function getModel($request) {
    global $CFG;
  
    $canonical = URL::getAdminCanonicalUrl($request->data);
    $rootUrl = substr(ADMIN_URL, 0, strlen(ADMIN_URL) - 1); // Remove last slash
    
    $ms = array(  );
    $pattern = '';
    foreach($CFG['SITE_LANGS'] as $lang) {
      $pattern .= '/'.$lang.'/|';
    }
    
    preg_match('#'.$rootUrl.'('.$pattern.'/)(.*?)$#', $canonical, $ms);
    
    $langs = array(  );
    
    foreach($CFG['SITE_LANGS'] as $lang) {
      $langs[$lang] = ADMIN_ROOT.$lang.'/'.(isset($ms[2]) ? $ms[2] : '');
    }
    
    $headerData = array( 
      'langs' => (object) $langs
     );
    
    return (object) array( "tpl" => "header", "data" => (object) $headerData );
  }
}
