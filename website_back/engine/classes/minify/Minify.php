<?php
class CN_Minifier {
  private static $phpCodesReplaced;

  public static function minifyTPL($file, $minFilePath) {
    $source = file_get_contents($file);
    
    $source = self::removePHPCodes($source);
    
    $source = self::minifyHTML($source);
    
    $source = self::restorePHPCodes($source);
    
    file_put_contents($minFilePath, $source);
    @chmod($minFilePath, 0666);
  }
  
  public static function restorePHPCodes($source) {
    if(count(self::$phpCodesReplaced) == 0) return $source;
  
    $phpCodeN = 0;
    $replacementCount = 0;
    do {
      if(!isset(self::$phpCodesReplaced[$phpCodeN])) break;
      $source = preg_replace('#{PHP_INLINE_CODE_(\d+)}#s', self::$phpCodesReplaced[$phpCodeN], $source, 1, $replacementCount);
      $phpCodeN++;
    } while($replacementCount > 0);
    
    return $source;
  }
  
  public static function removePHPCodes($source) {
    $ms = array();
    preg_match_all('#<\?(php|\=)(.*?)\?>#s', $source, $ms);
    self::$phpCodesReplaced = $ms[0];
    
    $phpCodeFound = 0;
    $replacementCount = 0;
    do {
      $source = preg_replace('#<\?(php|\=)(.*?)\?>#s', '{PHP_INLINE_CODE_'.$phpCodeFound.'}', $source, 1, $replacementCount);
      $phpCodeFound++;
    } while($replacementCount > 0);
    
    return $source;
  }
  
  public static function minifyHTML($source) {
    $search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
    );

    $replace = array(
        '>',
        '<',
        '\\1'
    );

    return preg_replace($search, $replace, $source);
  }

  /**
   * JS Minify
   */
  public static function minifyJS($files, $minFilePath) {
    $wholeSource = self::getSources($files);
    $minifiedCode = JSMin::minify($wholeSource, array('flaggedComments' => false));
    self::saveMinifiedJS($minifiedCode, $minFilePath);
  }

  private static function getSources($files){
    $tmFile = '';
    foreach($files as $file) {
      $tmFile .= file_get_contents($file);
      $tmFile .= "\n";
    }

    return $tmFile;
  }

  private static function saveMinifiedJS($code, $minFilePath){
    file_put_contents($minFilePath, $code);
    @chmod($minFilePath, 0666);
  }

  /**
   * CSS Minify
   */

  public static function minifyCss($files, $minFilePath) {
    $allMinCssSource = '';
    foreach($files as $filePath) {
      if(is_array($filePath)) {
        $media = isset($filePath['media']) ? $filePath['media'] : '';
        $filePath = isset($filePath['href']) ? $filePath['href'] : '';
      } else {
        $media = '';
      }
      
      if($filePath) {
        $allMinCssSource .= self::getMinifiedContent($filePath, $media);
      }
    }
    file_put_contents($minFilePath, $allMinCssSource);
    @chmod($minFilePath, 0666);
  }
  
  private static function getMinifiedContent($filePath, $media) {
    $result = '';

    if(file_exists($filePath)) {
      $cssCode = file_get_contents($filePath);
    } else {
      $cssCode = '';
    }
    $ms = array();
    preg_match_all('#@import\s[\'"](.*?)[\'"];#', $cssCode, $ms);
    
    $importsPath = preg_replace('#\/[a-zA-Z0-9_\-\.]+$#', '', $filePath).'/';

    foreach($ms[1] as $k => $file) {
      /* If imported files found, get their content */
      $subdirCount = substr_count($file, '/');
      $dirsMatch = '';
      for($i = 0; $i < $subdirCount; $i++) {
        $dirsMatch .= '../';
      }
      
      $tmpMinifiedCss = self::getMinifiedContent($importsPath.$file, $media); // Get minified content by recursive
      $result .= self::correctUrls($tmpMinifiedCss, $dirsMatch); // Correct urls like: turn ../../ to ../
      
      $cssCode = preg_replace('#'.$ms[0][$k].'#', '', $cssCode); // Remove @import "blabla.css";
    }

    if($media) $result .= '@media '.$media.' {';
    $result .= CssMin::minify($cssCode);
    if($media) $result .= '}';

    return $result;
  }
  
  private static function correctUrls($source, $needle) {
    if(!$needle) return $source;
    return str_replace('../', '', $source); // Quick fix. no relative paths for inline css
  
    $offset = 0;
    while(($pos = strpos($source, $needle, $offset)) !== false) {
      $offset = $pos + strlen($needle);
      
      while(($pos1 = strpos($source, $needle, $offset)) !== false) {
        if($pos1 - $pos <= strlen($needle)) {
          $offset = $pos1 + strlen($needle);
        } else {
          break;
        }
      }
      
      $part1 = substr($source, 0, $pos);
      $part2 = substr($source, $pos + strlen($needle));
      
      $source = $part1.$part2;
    }
    
    return $source;
  }
}

include __DIR__.'/cssmin.php';
include __DIR__.'/jsmin.php';
