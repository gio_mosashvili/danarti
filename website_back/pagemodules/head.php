<?php
class PageHead extends HeadModule {
  protected function getTitle($request) {
    if(isset($request->data) and $request->data == '404') return L('page_not_found');
  
    switch($request->module) {
      case 'home':
        return L('website_seo_title');
      case 'about':
        return L('about_seo_title');
      case 'events':
        return L('events_seo_title');
      case 'newspaper':
        return $this->escape($request->data->newspaper->title);
      case 'article':
        return $this->escape($request->data->article->title);
      case 'event':
        return $this->escape($request->data->event->title);
      default:
        return L('website_seo_title');
    }
  }
  
  protected function getDescription($request) {
    if(isset($request->data) and $request->data == '404') return L('page_not_found');
    
    switch($request->module) {
      case 'home':
        return L('website_seo_description');
      case 'about':
        return L('about_seo_description');
      case 'events':
        return L('events_seo_description');
      case 'newspaper':
        return $this->escape($request->data->newspaper->description);
      case 'article':
        return mb_substr($this->escape($request->data->article->text), 0, 500);
      case 'event':
        return mb_substr($this->escape($request->data->event->text), 0, 500);
      default:
        return L('website_seo_description');
    }
  }
  
  protected function getKeywords($request) {
    if(isset($request->data) and $request->data == '404') return L('page_not_found');
    
    switch($request->module) {
      case 'home':
        return L('website_seo_keywords');
      case 'about':
        return L('about_seo_keywords');
      case 'events':
        return L('events_seo_keywords');
      case 'newspaper':
        return str_replace(' ', ', ', $this->escape($request->data->newspaper->title));
      case 'article':
        return str_replace(' ', ', ', $this->escape($request->data->article->title));
      case 'event':
        return str_replace(' ', ', ', $this->escape($request->data->event->title));
      default:
        return L('website_seo_keywords');
    }
  }
  
  protected function getImage($request) {
    if(isset($request->data) and $request->data == '404') return ROOT_URL.'img/logo.png';
    
    switch($request->module) {
      default:
        return ROOT_URL.'img/logo.png';
    }
  }
}
