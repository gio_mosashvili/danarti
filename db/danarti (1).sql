-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2016 at 04:02 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `danarti`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_en`
--

CREATE TABLE `contact_en` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `fb_link` text NOT NULL,
  `inst_link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_en`
--

INSERT INTO `contact_en` (`id`, `address`, `tel`, `email`, `fb_link`, `inst_link`) VALUES
(1, 'ვაჟა ფშაველა 71', '212  211 1', 'ragaca@gmail.com', 'https://facebook.com', 'https://instagram.com');

-- --------------------------------------------------------

--
-- Table structure for table `contact_ka`
--

CREATE TABLE `contact_ka` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `tel` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `fb_link` text NOT NULL,
  `inst_link` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_ka`
--

INSERT INTO `contact_ka` (`id`, `address`, `tel`, `email`, `fb_link`, `inst_link`) VALUES
(1, 'ვაჟა ფშაველა 71', '212  211 1', 'ragaca@gmail.com', 'https://facebook.com', 'https://instagram.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_en`
--
ALTER TABLE `contact_en`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_ka`
--
ALTER TABLE `contact_ka`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_en`
--
ALTER TABLE `contact_en`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact_ka`
--
ALTER TABLE `contact_ka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
